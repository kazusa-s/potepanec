class Potepan::ProductsController < ApplicationController
  def index
    @categories = Spree::Taxonomy.includes(taxons: %i(children parent))
    @products = Spree::Product.includes(master: %i(images default_price))
  end

  def show
    @product = Spree::Product.find(params[:id])
  end
end
