class Potepan::CategoriesController < ApplicationController
  def show
    @categories = Spree::Taxonomy.includes(taxons: %i(children parent))
    @taxon = Spree::Taxon.find(params[:taxon_id])
    @products = @taxon.products.includes(master: %i(images default_price))
  end
end
