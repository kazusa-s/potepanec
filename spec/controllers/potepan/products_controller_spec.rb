require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #index" do
    let!(:product) { create(:product) }
    let!(:taxonomy) { create(:taxonomy) }

    before { get :index }

    it 'succeeds 200 response' do
      expect(response.status).to eq 200
    end

    it 'shows views/potepan/categories/index.html.erb' do
      expect(response).to render_template :index
    end

    it 'use @products, @categories' do
      expect(assigns(:products)).to eq [product]
      expect(assigns(:categories)).to eq [taxonomy]
    end
  end

  describe "GET #show" do
    let(:product) { create(:product) }

    before { get :show, params: { id: product.id } }

    it 'succeeds 200 response' do
      expect(response.status).to eq 200
    end
    it 'shows views/potepan/products/show.html.erb' do
      expect(response).to render_template :show
    end
    it 'uses @product' do
      expect(assigns(:product)).to eq product
    end
  end
end
