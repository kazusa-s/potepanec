require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "Get #show" do
    let!(:products) { taxon.products << create_list(:product, 3) }
    let!(:categories) { create_list(:taxonomy, 3) }
    let!(:taxonomy) { categories.first }
    let!(:taxon) { taxonomy.taxons.first }

    before { get :show, params: { taxon_id: taxon.id } }

    it 'succeeds 200 response' do
      expect(response.status).to eq 200
    end

    it 'shows views/potepan/categories/show.html.erb' do
      expect(response).to render_template :show
    end

    it 'use ＠taxon, @products, @categories' do
      expect(assigns(:categories)).to eq categories
      expect(assigns(:taxon)).to eq taxon
      expect(assigns(:products)).to eq products
    end
  end
end
