require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given(:product) { create(:product) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "visit potepan/products/show" do
    expect(page).to have_title "#{product.name} - BIGBAG Store"
    expect(page).to have_selector '.page-title > h2', text: "#{product.name}"
    expect(page).to have_selector '.breadcrumb > li.active', text: "#{product.name}"
    expect(page).to have_selector '.media-body > h2', text: "#{product.name}"
    expect(page).to have_selector '.media-body > h3', text: "#{product.display_price}"
    expect(page).to have_selector '.media-body > p',  text: "#{product.description}"
  end

  scenario "click header logo" do
    find('.navbar-brand').click
    expect(page).to have_title "BIGBAG Store"
  end

  scenario "check first home link inside header" do
    # Home linkが2つあるため最初にマッチするlinkを指定するため match: :first を指定
    click_link "Home", match: :first
    expect(page).to have_title "BIGBAG Store"
  end

  scenario "check second home link inside LightSection" do
    # 2つめの Home linkを指定するためにClass名でマッチさせてる。
    find('.breadcrumb li', match: :first).click
    expect(page).to have_title "BIGBAG Store"
  end

  scenario "check product_grid_left_sidebar link" do
    click_link "一覧ページへ戻る"
    expect(page).to have_content "Category name"
  end
end
