require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  given!(:categories) { [create(:taxonomy), create(:taxonomy, name: "Categories")] }
  given!(:taxonomy) { categories.first }
  given!(:taxon) { create(:taxon, name: "Mug", taxonomy: taxonomy) }
  given!(:taxon_2) { create(:taxon, name: "Bag", taxonomy: taxonomy) }
  given!(:product) { taxon.products << create(:product) }
  given!(:product_2) { taxon_2.products << create(:product) }

  before do
    visit potepan_category_path(taxon_id: taxon.id)
  end

  scenario "visit potepan/categories/taxon" do
    expect(page).to have_title "#{taxon.name} - BIGBAG Store"
    expect(page).to have_content product[0].name
    expect(page).to have_content categories[0].name
    expect(page).to have_content categories[1].name
  end

  scenario "visit potepan/products/show" do
    click_on "#{product[0].name}"
    expect(page).to have_title "#{product[0].name} - BIGBAG Store"
    expect(page).to have_selector '.page-title > h2', text: "#{product[0].name}"
    expect(page).to have_selector '.breadcrumb > li.active', text: "#{product[0].name}"
    expect(page).to have_selector '.media-body > h2', text: "#{product[0].name}"
    expect(page).to have_selector '.media-body > h3', text: "#{product[0].display_price}"
    expect(page).to have_selector '.media-body > p',  text: "#{product[0].description}"
  end

  scenario "visit taxon_2 page" do
    visit potepan_category_path(taxon_id: taxon_2.id)
    expect(page).to have_title "#{taxon_2.name} - BIGBAG Store"
    expect(page).to have_content product_2[0].name
    expect(page).not_to have_title "#{taxon.name} - BIGBAG Store"
    expect(page).not_to have_content product[0].name
  end
end
